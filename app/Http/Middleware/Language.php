<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = config('app.languages.default');

        if ($lang = Cookie::get('language')) {
            $locale = $lang;
        }

        app()->setLocale($locale);

        return $next($request);
    }
}
