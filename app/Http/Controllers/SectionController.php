<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSectionRequest;
use App\Http\Requests\UpdateSectionRequest;
use App\Section;
use App\User;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('crud.sections.index', [
            'sections' => Section::with('users')->latest()->paginate(7)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        return view('crud.sections.create', [
            'users' => User::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSectionRequest $request
     * @return RedirectResponse
     */
    public function store(StoreSectionRequest $request): RedirectResponse
    {
        DB::transaction(function () use ($request) {
            $section = Section::create([
                'name' => $request->title,
                'description' => $request->description,
                'logo' => $request->file('logo')->store('logo')
            ]);

            $section->users()->sync($request->users);
        });

        return redirect()
            ->route('sections.index')
            ->with('message', __('custom.crud.added', ['entity' => __('custom.user')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Section $section
     * @return Renderable
     */
    public function edit(Section $section): Renderable
    {
        $sectionUsers = $section->users->pluck('id');

        $users = User::all()->map(function ($user) use ($sectionUsers) {
            if ($sectionUsers->contains($user->id)) {
                $user->setAttribute('checked', true);
            } else {
                $user->setAttribute('checked', false);
            }
            return $user;
        });

        return view('crud.sections.edit', [
            'section' => $section,
            'users' => $users
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSectionRequest $request
     * @param Section $section
     * @return RedirectResponse
     */
    public function update(UpdateSectionRequest $request, Section $section): RedirectResponse
    {
        $data = ['name' => $request->title, 'description' => $request->description];

        if ($request->file('logo')) {
            $data['logo'] = $request->file('logo')->store('logo');
        }

        DB::transaction(function () use ($request, $section, $data) {
            $section->update($data);
            $section->users()->sync($request->users);
        });

        return redirect()
            ->route('sections.index')
            ->with('message', __('custom.crud.updated', ['entity' => __('custom.section')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Section $section
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Section $section): RedirectResponse
    {
        $section->delete();

        return redirect()
            ->route('sections.index')
            ->with('message', __('custom.crud.deleted', ['entity' => __('custom.user')]));
    }
}
