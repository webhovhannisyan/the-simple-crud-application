<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    /**
     * Supported languages
     * @var array $supportedLanguages
     */
    protected $supportedLanguages = [];

    /**
     * Init languages
     */
    public function __construct()
    {
        $this->supportedLanguages = config('app.languages.supported');
    }

    /**
     * Change language
     *
     * @param $lang
     * @return RedirectResponse
     */
    public function index($lang): RedirectResponse
    {
        if (in_array($lang, $this->supportedLanguages, true)) {
            return redirect()->back()->withCookie(
                cookie()->forever('language', $lang)
            );
        }

        return redirect()->back();
    }
}
