<?php

use App\User;
use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Section::class, 15)->create()->each(function ($section) {
            $userIds = User::inRandomOrder()->limit(3)->get()->pluck('id');
            $section->users()->syncWithoutDetaching($userIds);
        });
    }
}
