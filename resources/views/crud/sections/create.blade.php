@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 pt-4 pb-4 d-flex justify-content-between">
                <h4>{{ __('custom.sections') }}</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12">
                <form action="{{ route('sections.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="title" class="col-12 col-form-label">{{ __('custom.title') }}</label>

                        <div class="col-12">
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}">

                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="description" class="col-12 col-form-label">{{ __('custom.description') }}</label>

                        <div class="col-12">
                            <textarea class="form-control @error('description') is-invalid @enderror" id="description" rows="3" name="description">{{ old('description') }}</textarea>

                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <input type="file" name="logo" class="@error('logo') is-invalid @enderror">

                            @error('logo')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <h4>{{ __('custom.users') }}</h4>

                            @foreach($users as $user)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="{{ $user->id }}" id="checkbox-{{ $user->id }}" name="users[]">
                                    <label class="form-check-label" for="checkbox-{{ $user->id }}">
                                        {{ $user->name }}
                                    </label>
                                </div>
                            @endforeach

                            @error('users')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">
                                {{ __('add') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
