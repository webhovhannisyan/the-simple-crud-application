<?php

return [
    'ru' => 'Рус',
    'en' => 'Eng',
    'title' => 'Title',
    'description' => 'Description',
    'logo' => 'Logo',
    'add' => 'Add',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'created_at' => 'Created at',
    'actions' => 'Actions',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'sections' => 'Sections',
    'section' => 'Section',
    'users' => 'Users',
    'user' => 'User',
    'nothing_found' => 'Nothing found',
    'crud' => [
        'added' => ':entity successfully saved!',
        'deleted' => ':entity successfully deleted!',
        'updated' => ':entity successfully updated!',
    ]
];
